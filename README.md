# About

libcnce is a C++ library that describes serial data message structures
to control a numeric device.


## Copying

libcnce is distributed under the terms in the LICENSE-libcnce.txt text file
that comes with this package.


## Installation

libcnce uses the CMake build system.

For a system wide installation call:

```
mkdir build-libcnce
cd build-libcnce
cmake < libcnce_git_dir>
make -j5
sudo make install
```
