/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_sensors_32_hpp_
#define __inc_cnce_frog_message_in_state_sensors_32_hpp_

#include "message_in_state.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State_Sensors_32
///
class Frog_Message_In_State_Sensors_32 : public ::cnce::Frog_Message_In_State
{
  // Public methods
  public:
  void
  init_header ();

  void
  reset ();

  // Public data
  public:
  /// @brief Every bit represents the state (high/low) of the respective sensor
  uint8_t sensors_states[ 4 ];
};

inline void
Frog_Message_In_State_Sensors_32::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_IN_STATE_SENSORS_32 );
}

inline void
Frog_Message_In_State_Sensors_32::reset ()
{
  uint8_t * ptr ( (uint8_t *)( this ) + sizeof ( ::cnce::Frog_Message_In ) );
  const unsigned int num ( sizeof ( ::cnce::Frog_Message_In_State_Sensors_32 ) -
                           sizeof ( ::cnce::Frog_Message_In ) );
  for ( unsigned int ii = 0; ii != num; ++ii ) {
    ptr[ ii ] = 0;
  }
}

} // namespace cnce

#endif
