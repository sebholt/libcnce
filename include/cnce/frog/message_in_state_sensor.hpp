/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_sensor_hpp_
#define __inc_cnce_frog_message_in_state_sensor_hpp_

#include "message_in_state.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State_Sensor
///
class Frog_Message_In_State_Sensor : public ::cnce::Frog_Message_In_State
{
  // Public methods
  public:
  void
  init_header ();

  void
  reset ();

  // Public data
  public:
  uint8_t sensor_index;
  uint8_t sensor_state;
};

inline void
Frog_Message_In_State_Sensor::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_IN_STATE_SENSOR );
}

inline void
Frog_Message_In_State_Sensor::reset ()
{
  uint8_t * ptr ( (uint8_t *)( this ) + sizeof ( ::cnce::Frog_Message_In ) );
  const unsigned int num ( sizeof ( ::cnce::Frog_Message_In_State_Sensor ) -
                           sizeof ( ::cnce::Frog_Message_In ) );
  for ( unsigned int ii = 0; ii != num; ++ii ) {
    ptr[ ii ] = 0;
  }
}

} // namespace cnce

#endif
