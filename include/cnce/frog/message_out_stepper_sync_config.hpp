/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_stepper_sync_config_hpp__
#define __inc_cnce_frog_message_out_stepper_sync_config_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Stepper_Sync_Config
///
class Frog_Message_Out_Stepper_Sync_Config : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public attributes
  public:
  /// @brief Index of the stepper sync group
  uint8_t sync_group_index;
  /// @brief Little endian bit list of steppers to register in the group
  uint8_t stepper_register[ 4 ];
};

inline void
Frog_Message_Out_Stepper_Sync_Config::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_STEPPER_SYNC_CONFIG );
}

} // namespace cnce

#endif
