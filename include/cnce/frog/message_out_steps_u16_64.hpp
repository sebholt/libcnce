/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_steps_u16_64_hpp__
#define __inc_cnce_frog_message_out_steps_u16_64_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"
#include "step_type.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Steps_U16_64
///
class Frog_Message_Out_Steps_U16_64 : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public data
  public:
  // sizeof(Frog_Message_Out_Steps_U16_64) should be 64
  static const uint8_t num_usecs_bytes = 56;
  static const uint8_t num_usecs = num_usecs_bytes / 2;

  uint8_t stepper_index;
  /// @brief One of ::cnce::Frog_Step_Type
  uint8_t step_type;
  uint8_t step_flags;
  /// @brief Series of little endian unsigned 16 bit integers
  uint8_t usecs[ num_usecs_bytes ];
};

inline void
Frog_Message_Out_Steps_U16_64::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_STEPS_U16_64 );
}

} // namespace cnce

#endif
