/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_fader_16_hpp__
#define __inc_cnce_frog_message_out_fader_16_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Fader_16
///
class Frog_Message_Out_Fader_16 : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public data
  public:
  uint8_t fader_index;
  /// @brief Little endian signed or unsigned 16 bit integer value
  uint8_t fader_value[ 2 ];
};

inline void
Frog_Message_Out_Fader_16::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_FADER_16 );
}

} // namespace cnce

#endif
