/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_output_type_hpp__
#define __inc_cnce_frog_output_type_hpp__

namespace cnce
{

enum Frog_Output_Type
{
  FROG_OUTPUT_UNUSED,
  FROG_OUTPUT_SWITCH_PLAIN,
  FROG_OUTPUT_SWITCH_FREQUENCY,
  FROG_OUTPUT_STEPPER_DIRECTION,
  FROG_OUTPUT_STEPPER_CLOCK
};

} // namespace cnce

#endif
