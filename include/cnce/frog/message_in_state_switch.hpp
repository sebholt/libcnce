/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_switch_hpp_
#define __inc_cnce_frog_message_in_state_switch_hpp_

#include "message_in_state.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State_Switch
///
class Frog_Message_In_State_Switch : public ::cnce::Frog_Message_In_State
{
  // Public methods
  public:
  void
  init_header ();

  void
  reset ();

  // Public data
  public:
  uint8_t switch_index;
  uint8_t switch_state;
};

inline void
Frog_Message_In_State_Switch::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_IN_STATE_SWITCH );
}

inline void
Frog_Message_In_State_Switch::reset ()
{
  uint8_t * ptr ( (uint8_t *)( this ) + sizeof ( ::cnce::Frog_Message_In ) );
  const unsigned int num ( sizeof ( ::cnce::Frog_Message_In_State_Switch ) -
                           sizeof ( ::cnce::Frog_Message_In ) );
  for ( unsigned int ii = 0; ii != num; ++ii ) {
    ptr[ ii ] = 0;
  }
}

} // namespace cnce

#endif
