/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_messages_in_hpp__
#define __inc_cnce_frog_messages_in_hpp__

#include "message_in_state_fader_16.hpp"
#include "message_in_state_fader_32.hpp"
#include "message_in_state_fader_8.hpp"
#include "message_in_state_sensor.hpp"
#include "message_in_state_sensor_16.hpp"
#include "message_in_state_sensor_32.hpp"
#include "message_in_state_sensor_8.hpp"
#include "message_in_state_sensors_32.hpp"
#include "message_in_state_stepper.hpp"
#include "message_in_state_switch.hpp"
#include "message_in_state_switches_32.hpp"

#endif
