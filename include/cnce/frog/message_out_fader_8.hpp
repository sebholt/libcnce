/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_fader_8_hpp__
#define __inc_cnce_frog_message_out_fader_8_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Fader_8
///
class Frog_Message_Out_Fader_8 : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public data
  public:
  uint8_t fader_index;
  /// @brief Little endian signed or unsigned 8 bit integer value
  uint8_t fader_value;
};

inline void
Frog_Message_Out_Fader_8::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_FADER_8 );
}

} // namespace cnce

#endif
