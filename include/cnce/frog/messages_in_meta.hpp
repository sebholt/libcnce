/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_messages_in_meta_hpp__
#define __inc_cnce_frog_messages_in_meta_hpp__

#include <inttypes.h>

namespace cnce
{

///
/// @brief HOST / PC incoming message types
///
enum Frog_Message_In_Type
{
  FROG_MESSAGE_IN_NULL = 0,

  FROG_MESSAGE_IN_STATE_SENSOR,
  FROG_MESSAGE_IN_STATE_SENSOR_8,
  FROG_MESSAGE_IN_STATE_SENSOR_16,
  FROG_MESSAGE_IN_STATE_SENSOR_32,
  FROG_MESSAGE_IN_STATE_SWITCH,
  FROG_MESSAGE_IN_STATE_FADER_8,
  FROG_MESSAGE_IN_STATE_FADER_16,
  FROG_MESSAGE_IN_STATE_FADER_32,
  FROG_MESSAGE_IN_STATE_STEPPER,

  FROG_MESSAGE_IN_STATE_SENSORS_32,
  FROG_MESSAGE_IN_STATE_SWITCHES_32,

  // Range all
  FROG_MESSAGE_IN_TYPE_LIST_END,
  FROG_MESSAGE_IN_TYPE_LIST_BEGIN = ( FROG_MESSAGE_IN_NULL + 1 )
};

/// @brief Number of message types
///
const uint8_t frog_message_in_num_types ( FROG_MESSAGE_IN_TYPE_LIST_END -
                                          FROG_MESSAGE_IN_TYPE_LIST_BEGIN );

/// @brief Maps message type to index
///
inline uint8_t
frog_message_in_type_index ( uint8_t type_n )
{
  return ( type_n - ( uint8_t )::cnce::FROG_MESSAGE_IN_TYPE_LIST_BEGIN );
}

/// @brief Maps message index to type
///
inline uint8_t
frog_message_in_index_type ( uint8_t index_n )
{
  return ( ( uint8_t )::cnce::FROG_MESSAGE_IN_TYPE_LIST_BEGIN + index_n );
}

/// @brief Checks if the message type is valid
///
inline bool
frog_message_in_type_valid ( uint8_t type_n )
{
  return ( ( type_n != ::cnce::FROG_MESSAGE_IN_NULL ) &&
           ( type_n < ::cnce::FROG_MESSAGE_IN_TYPE_LIST_END ) );
}

} // namespace cnce

#endif
