/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_stepper_sync_hpp__
#define __inc_cnce_frog_message_out_stepper_sync_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"
#include "stepper_sync_type.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Stepper_Sync
///
class Frog_Message_Out_Stepper_Sync : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public attributes
  public:
  /// @brief Index of the sync group
  uint8_t sync_group_index;
  /// @brief One of ::cnce::Frog_Stepper_Sync_Type
  uint8_t sync_type;
};

inline void
Frog_Message_Out_Stepper_Sync::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_STEPPER_SYNC );
}

} // namespace cnce

#endif
