/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_fader_16_hpp__
#define __inc_cnce_frog_message_in_state_fader_16_hpp__

#include "message_in_state.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State_Fader_16
///
class Frog_Message_In_State_Fader_16 : public ::cnce::Frog_Message_In_State
{
  // Public methods
  public:
  void
  init_header ();

  // Public data
  public:
  uint8_t fader_index;
  /// @brief Little endian signed or unsigned 16 bit integer value
  uint8_t fader_value[ 2 ];
};

inline void
Frog_Message_In_State_Fader_16::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_IN_STATE_FADER_16 );
}

} // namespace cnce

#endif
