/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_step_type_hpp__
#define __inc_cnce_frog_step_type_hpp__

namespace cnce
{

/// @brief Frog_Step_Type
///
enum Frog_Step_Type
{
  // Invalid job type
  FROG_STEP_TYPE_INVALID,

  // Movement
  FROG_STEP_TYPE_HOLD,
  FROG_STEP_TYPE_FORWARD,
  FROG_STEP_TYPE_BACKWARD,

  // Switches
  FROG_STEP_TYPE_SWITCH_SET_LOW,
  FROG_STEP_TYPE_SWITCH_SET_HIGH,

  // List end
  FROG_STEP_TYPE_LIST_END
};

inline bool
frog_step_type_valid ( uint8_t step_type_n )
{
  return ( ( step_type_n != FROG_STEP_TYPE_INVALID ) &&
           ( step_type_n < FROG_STEP_TYPE_LIST_END ) );
}

} // namespace cnce

#endif
