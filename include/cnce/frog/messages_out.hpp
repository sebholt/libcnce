/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_messages_out_hpp__
#define __inc_cnce_messages_out_hpp__

#include "message_out_fader_16.hpp"
#include "message_out_fader_32.hpp"
#include "message_out_fader_8.hpp"
#include "message_out_generator_frequency.hpp"
#include "message_out_heartbeat.hpp"
#include "message_out_output_config.hpp"
#include "message_out_stepper_sync.hpp"
#include "message_out_stepper_sync_config.hpp"
#include "message_out_steps_u16_64.hpp"
#include "message_out_switch.hpp"

#endif
