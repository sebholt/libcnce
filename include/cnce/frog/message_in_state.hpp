/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_hpp_
#define __inc_cnce_frog_message_in_state_hpp_

#include "message_in.hpp"
#include "messages_in_meta.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State
///
class Frog_Message_In_State : public ::cnce::Frog_Message_In
{
  // Public methods
  public:
  // Public data
  public:
  /// @brief Little endian unsigned 16 bit integer
  uint8_t latest_message_uid[ 2 ];
};

} // namespace cnce

#endif
