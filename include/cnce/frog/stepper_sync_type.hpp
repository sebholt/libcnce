/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_stepper_sync_type_hpp__
#define __inc_cnce_frog_stepper_sync_type_hpp__

namespace cnce
{

enum Frog_Stepper_Sync_Type
{
  FROG_STEPPER_SYNC_NULL,
  FROG_STEPPER_SYNC_LOCK_INSTALL,
  FROG_STEPPER_SYNC_LOCK_RELEASE
};

} // namespace cnce

#endif
