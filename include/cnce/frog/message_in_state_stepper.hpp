/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_stepper_hpp_
#define __inc_cnce_frog_message_in_state_stepper_hpp_

#include "message_in_state.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State_Stepper
///
class Frog_Message_In_State_Stepper : public ::cnce::Frog_Message_In_State
{
  // Public methods
  public:
  void
  init_header ();

  void
  reset ();

  // Public data
  public:
  uint8_t stepper_index;
  /// @brief Little endian unsigned 16 bit integer
  uint8_t steps_queue_length[ 2 ];
};

inline void
Frog_Message_In_State_Stepper::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_IN_STATE_STEPPER );
}

inline void
Frog_Message_In_State_Stepper::reset ()
{
  uint8_t * ptr ( (uint8_t *)( this ) + sizeof ( ::cnce::Frog_Message_In ) );
  const unsigned int num ( sizeof ( ::cnce::Frog_Message_In_State_Stepper ) -
                           sizeof ( ::cnce::Frog_Message_In ) );
  for ( unsigned int ii = 0; ii != num; ++ii ) {
    ptr[ ii ] = 0;
  }
}

} // namespace cnce

#endif
