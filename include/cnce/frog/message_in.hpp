/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_hpp__
#define __inc_cnce_frog_message_in_hpp__

#include <inttypes.h>

namespace cnce
{

///
/// @brief HOST / PC incoming message header bytes
///
const uint8_t frog_message_in_header[] = {'f', 'h'};
const uint8_t frog_message_in_header_size ( sizeof ( frog_message_in_header ) );

/// @brief Frog_Message_In
///
class Frog_Message_In
{
  // Protected methods
  protected:
  void
  init_header_type ( uint8_t message_type_n );

  // Public data
  public:
  uint8_t message_header[::cnce::frog_message_in_header_size ];
  /// @brief Must come right after the header
  uint8_t message_type;
  /// @brief Little endian unsigned 16 bit integer
  uint8_t message_uid[ 2 ];
};

inline void
Frog_Message_In::init_header_type ( uint8_t message_type_n )
{
  for ( unsigned int ii = 0; ii != ::cnce::frog_message_in_header_size; ++ii ) {
    message_header[ ii ] = ::cnce::frog_message_in_header[ ii ];
  }
  message_type = message_type_n;
}

} // namespace cnce

#endif
