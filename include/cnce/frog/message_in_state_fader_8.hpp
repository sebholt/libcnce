/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_in_state_fader_8_hpp__
#define __inc_cnce_frog_message_in_state_fader_8_hpp__

#include "message_in_state.hpp"

namespace cnce
{

/// @brief Frog_Message_In_State_Fader_8
///
class Frog_Message_In_State_Fader_8 : public ::cnce::Frog_Message_In_State
{
  // Public methods
  public:
  void
  init_header ();

  // Public data
  public:
  uint8_t fader_index;
  /// @brief Little endian signed or unsigned 8 bit integer value
  uint8_t fader_value;
};

inline void
Frog_Message_In_State_Fader_8::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_IN_STATE_FADER_8 );
}

} // namespace cnce

#endif
