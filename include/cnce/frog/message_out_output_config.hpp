/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_output_config_hpp__
#define __inc_cnce_frog_message_out_output_config_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"
#include "output_type.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Output_Config
///
class Frog_Message_Out_Output_Config : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public attributes
  public:
  uint8_t output_index;
  uint8_t output_type;
  /// @brief Axis index or switch index
  uint8_t stepper_switch_index;
  uint8_t freq_gen_index;
};

inline void
Frog_Message_Out_Output_Config::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_OUTPUT_CONFIG );
}

} // namespace cnce

#endif
