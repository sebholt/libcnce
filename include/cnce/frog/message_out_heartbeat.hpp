/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_heartbeat_hpp__
#define __inc_cnce_frog_message_out_heartbeat_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Heartbeat
///
class Frog_Message_Out_Heartbeat : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();
};

inline void
Frog_Message_Out_Heartbeat::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_HEARTBEAT );
}

} // namespace cnce

#endif
