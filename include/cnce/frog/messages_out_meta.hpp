/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_messages_out_meta_hpp__
#define __inc_cnce_frog_messages_out_meta_hpp__

#include <inttypes.h>

namespace cnce
{

///
/// @brief HOST / PC outgoing message types
///
enum Frog_Message_Out_Type
{
  FROG_MESSAGE_OUT_NULL = 0,

  FROG_MESSAGE_OUT_HEARTBEAT,
  FROG_MESSAGE_OUT_STEPS_U16_64,
  FROG_MESSAGE_OUT_STEPPER_SYNC_CONFIG,
  FROG_MESSAGE_OUT_STEPPER_SYNC,
  FROG_MESSAGE_OUT_SWITCH,
  FROG_MESSAGE_OUT_FADER_8,
  FROG_MESSAGE_OUT_FADER_16,
  FROG_MESSAGE_OUT_FADER_32,
  FROG_MESSAGE_OUT_OUTPUT_CONFIG,
  FROG_MESSAGE_OUT_GENERATOR_FREQUENCY,

  // Range all
  FROG_MESSAGE_OUT_TYPE_LIST_END,
  FROG_MESSAGE_OUT_TYPE_LIST_BEGIN = ( FROG_MESSAGE_OUT_NULL + 1 ),
};

/// @brief Number of message types
///
const uint8_t frog_message_out_num_types ( FROG_MESSAGE_OUT_TYPE_LIST_END -
                                           FROG_MESSAGE_OUT_TYPE_LIST_BEGIN );

/// @brief Maps message type to index
///
inline uint8_t
frog_message_out_type_index ( uint8_t type_n )
{
  return ( type_n - ( uint32_t )::cnce::FROG_MESSAGE_OUT_TYPE_LIST_BEGIN );
}

/// @brief Maps message index to type
///
inline uint8_t
frog_message_out_index_type ( uint8_t index_n )
{
  return ( ( uint8_t )::cnce::FROG_MESSAGE_OUT_TYPE_LIST_BEGIN + index_n );
}

/// @brief Checks if the message type is valid
///
inline bool
frog_message_out_type_valid ( uint8_t type_n )
{
  return ( ( type_n != ::cnce::FROG_MESSAGE_OUT_NULL ) &&
           ( type_n < ::cnce::FROG_MESSAGE_OUT_TYPE_LIST_END ) );
}

} // namespace cnce

#endif
