/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

#ifndef __inc_cnce_frog_message_out_output_frequency_hpp__
#define __inc_cnce_frog_message_out_output_frequency_hpp__

#include "message_out.hpp"
#include "messages_out_meta.hpp"

namespace cnce
{

/// @brief Frog_Message_Out_Generator_Frequency
///
class Frog_Message_Out_Generator_Frequency : public ::cnce::Frog_Message_Out
{
  // Public methods
  public:
  void
  init_header ();

  // Public attributes
  public:
  /// @brief Frequency generator index
  uint8_t generator_index;
  /// @brief Little endian unsigned 32 bit integer
  uint8_t pulse_usecs_total[ 4 ];
  /// @brief Little endian unsigned 32 bit integer
  uint8_t pulse_usecs_high[ 4 ];
};

inline void
Frog_Message_Out_Generator_Frequency::init_header ()
{
  init_header_type ( ::cnce::FROG_MESSAGE_OUT_GENERATOR_FREQUENCY );
}

} // namespace cnce

#endif
