/// libcnce is a C++ library that describes serial data message structures
/// to control a numeric device.

// This file marks the root of libcnce

#include "cnce/frog/messages_in.hpp"
#include "cnce/frog/messages_out.hpp"
