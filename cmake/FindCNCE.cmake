#.rst:
# FindCNCE
# --------
#
# Find CNCE
#
# Find the CNCE includes. This module defines
#
# ::
#
#   CNCE_INCLUDE_DIR, where to find cnce_lib.hpp, etc.
#   CNCE_FOUND, If false, do not try to use CNCE.

include ( FindPackageHandleStandardArgs )

# --- Find headers directory
set ( CNCE_SEARCH_HEADER_PATHS
  ${CNCE_SEARCH_HEADER_PATHS}
  "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_INCLUDE@" )
find_path (
  CNCE_INCLUDE_DIR
  "cnce-lib.hpp"
  PATHS ${CNCE_SEARCH_HEADER_PATHS} )

# handle the QUIETLY and REQUIRED arguments
# and set MTM_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS (
    CNCE DEFAULT_MSG CNCE_INCLUDE_DIR )

mark_as_advanced (
  CNCE_SEARCH_HEADER_PATHS )

if ( CNCE_FOUND AND NOT TARGET cnce::cnce )
  add_library ( cnce::cnce INTERFACE IMPORTED )
  set_target_properties ( cnce::cnce PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${CNCE_INCLUDE_DIR}"
  )
endif()
