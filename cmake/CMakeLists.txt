include ( CheckIncludeFiles )

# --- File configuration
configure_file (
    ${CMAKE_CURRENT_SOURCE_DIR}/FindCNCE.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/FindCNCE.cmake @ONLY )

# --- Installation
install ( FILES
    ${CMAKE_CURRENT_BINARY_DIR}/FindCNCE.cmake
    DESTINATION ${INSTALL_DIR_CMAKE} )
